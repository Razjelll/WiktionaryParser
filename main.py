from interwiktionary_mapper.mapper import InterwiktionaryMapper
from parser.values import Languages
from parser.wiktionary_parser import WiktionaryParser
import pickle
from register_getter import RegisterGetter
from register_fitter import RegisterFitter


POLISH_PARSER_PATH = '/media/roman/SeagateExpansion/Pobrane/Wiktionary/Pliki/plwiktionary-20190701-pages-articles.xml'
# POLISH_PARSER_PATH = '/media/roman/SeagateExpansion/Pobrane/Wiktionary/Pliki/test.xml'
# POLISH_PARSER_PATH = '/media/roman/SeagateExpansion/Pobrane/Wiktionary/Pliki/test2.xml'
# ENGLISH_PARSER_PATH = '/media/roman/SeagateExpansion/Pobrane/Wiktionary/Pliki/enwiktionary-20190701-pages-articles.xml'
# ENGLISH_PARSER_PATH = '/media/roman/SeagateExpansion/Pobrane/Wiktionary/Pliki/test_en2.xml'
ENGLISH_PARSER_PATH = '/media/roman/SeagateExpansion/Pobrane/Wiktionary/Pliki/dog_translations.xml'

# POLISH_PARSER_RESULT_PATH = 'output/polish_wiki.pck'
POLISH_PARSER_RESULT_PATH = 'output/polish_wiki_eng.pck'
ENGLISH_PARSER_RESULT_PATH = 'output/english_wiki.pck'
POLISH_REGISTER_PATH = 'output/polish_registers.txt'
ENGLISH_REGISTER_PATH = 'output/english_registers.txt'


def main():
    # _start_polish_parser()
    _start_english_parser()

    # _start_polish_registers()
    # _start_english_registers()

    # _start_register_fitter()
    # _clean_register_file()

    # _set_registers_numbers()

    # _map_inter_wiktionary()

def _start_polish_parser():
    parser = WiktionaryParser(Languages.POLISH)
    result = parser.parse(POLISH_PARSER_PATH)
    _save_result_to_file(result, POLISH_PARSER_RESULT_PATH)


def _start_english_parser():
    parser = WiktionaryParser(Languages.ENGLISH)
    result = parser.parse(ENGLISH_PARSER_PATH)
    _save_result_to_file(result, ENGLISH_PARSER_RESULT_PATH)

def _save_result_to_file(result, path):
    pickle.dump(result, open(path, 'wb'))

def _read_result_from_file(path):
    return pickle.load(open(path, 'rb'))

def _start_polish_registers():
    parser_result = pickle.load(open(POLISH_PARSER_RESULT_PATH, 'rb'))
    registers = RegisterGetter.get_polish_registers(parser_result)
    RegisterGetter.save_to_file(registers, POLISH_REGISTER_PATH)

def _start_english_registers():
    parser_result = pickle.load(open(ENGLISH_PARSER_RESULT_PATH, 'rb'))
    registers = RegisterGetter.get_english_registers(parser_result)
    RegisterGetter.save_to_file(registers, ENGLISH_REGISTER_PATH)

def _start_register_fitter():
    polish_path = 'output/polish_registers_1.txt'
    english_path = 'output/english_registers.txt'

    registers_mapping = RegisterFitter.fit(polish_path, english_path)
    RegisterFitter.save_register_mappings_to_file(registers_mapping, 'output/registers_mapping.txt')

def _clean_register_file():
    # input_path = 'output/english_registers.txt'
    # output_path = 'output/english_registers_clean.txt'

    input_path = 'output/translations.txt'
    output_path = 'output/translations_clean.txt'

    RegisterFitter.clean_register_file(input_path, output_path)

def _get_polish_parser_result_from_file():
    return pickle.load(open(POLISH_PARSER_RESULT_PATH, 'rb'))

def _get_english_parser_result_from_file():
    return pickle.load(open(ENGLISH_PARSER_RESULT_PATH, 'rb'))

def _set_registers_numbers():
    mapping_path = 'output/registers_mapping.txt'
    mappings = RegisterFitter.get_registers_mappings_from_file(mapping_path)

    polish_parser_result = _get_polish_parser_result_from_file()
    english_parser_result = _get_english_parser_result_from_file()

    RegisterFitter.connect_registers(mappings, english_parser_result, polish_parser_result)
    # registers_numbers_map = RegisterFitter.get_register_number(mappings)
    # RegisterFitter.find_registers_mapping_conflict(registers_numbers_map)
    # print(registers_numbers_map)

def _map_inter_wiktionary():
    first_wiki_path = 'output/polish_wiki_eng.pck'
    second_wiki_path = ENGLISH_PARSER_RESULT_PATH
    mapper = InterwiktionaryMapper()
    mapper.start(first_wiki_path, second_wiki_path)

main()
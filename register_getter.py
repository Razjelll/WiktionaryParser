from collections import Counter
import operator

class RegisterGetter:


    @staticmethod
    def get_polish_registers(parser_result):
        # TODO: sprawdzić, czy w polskim parserze może wystepować wiele rejestrów
        registers = list()
        for word, meanings in parser_result.items():
            for meaning in meanings:
                register = meaning.register
                if register and RegisterGetter._is_valid_register(register):
                    registers.append(register)
        return registers

    @staticmethod
    def get_english_registers(parser_result):
        registers = list()

        for word, meanings in parser_result.items():
            for meaning in meanings:
                register = meaning.register
                if register and register.startswith('lb|en|'):
                    value = register.split('|',2)[2]
                    for v in value.split('|'):
                        if RegisterGetter._is_valid_register(v) and ' ' not in v:
                            registers.append(v)

        return registers

    @staticmethod
    def _is_valid_register(register):
        return '|' not in register and not any(i.isdigit() for i in register) and register.lower() == register

    @staticmethod
    def save_to_file(registers, path):
        sorted_x = RegisterGetter._get_most_often_used_registers(registers)
        RegisterGetter._save_registers_to_file(path, sorted_x)

    @staticmethod
    def _save_registers_to_file(path, sorted_x):
        with open(path, 'w') as file:
            for x in sorted_x:
                file.write(x[0])
                file.write(': ')
                file.write(str(x[1]))
                file.write('\n')

    @staticmethod
    def _get_most_often_used_registers(registers):
        counter_map = {}
        used = set()
        for register in registers:
            print(register)
            if register not in used:
                count = registers.count(register)
                counter_map[register] = count
                used.add(register)
        sorted_x = sorted(counter_map.items(), key=lambda kv: -kv[1])
        return sorted_x

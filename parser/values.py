
class Gender:
    MUSCULINE = 0
    FEMININE = 1
    NEUTRAL = 2
    LACK = -1

class PartOfSpeech:
    NOUN = 0
    VERB = 1
    ADJECTIVE = 2
    ADVERB = 3
    LACK = -1

class Languages:
    POLISH = 'pl'
    ENGLISH = 'en'
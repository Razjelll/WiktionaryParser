import regex as re

from parser.i_parser import IParser
from parser.models import Meaning
from parser.values import Gender, PartOfSpeech, Languages

START_SECTION = '{{'

START_LANGUAGE_SECTION = '=='


class ParseResult:

    def __init__(self, number=None):
        self.number = number


class MeaningResult(ParseResult):

    def __init__(self, number=None, pos=None, gender=None, register=None, definition=None):
        super().__init__(number)
        self.pos = pos
        self.gender = gender
        self.register = register
        self.definition = definition


class TranslationResult(ParseResult):

    def __init__(self, number=None, language=None, translations=None):
        super().__init__(number)
        self.language = language
        self.translations = translations
        if translations is None:
            self.translations = []


class ElementResult(ParseResult):

    def __init__(self, number=None, elements=None):
        super().__init__(number)
        if elements is None:
            elements = []
        self.elements = elements


class MeaningSetter:

    @staticmethod
    def set_meanings_result(results_list: list, meanings: dict):
        for result in results_list:
            meaning = Meaning()
            meaning.part_of_speech = result.pos
            meaning.gender = result.gender
            meaning.register = result.register
            meaning.definition = result.definition

            meanings[result.number] = meaning

    @staticmethod
    def set_examples_result(results_list: list, meanings: dict):
        for result in results_list:
            number = result.number
            if number in meanings:
                meaning = meanings[number]
                example = MeaningSetter._create_text_from_list_elements(result)
                meaning.examples.append(example)  # TODO: sprawdzić, czy to działa poprawnie

    @staticmethod
    def _create_text_from_list_elements(result):
        return ' '.join(result.elements)

    @staticmethod
    def set_relations_result(results_list: list, meanings: dict, relations_type):
        for result in results_list:
            number = result.number
            if number in meanings:
                meaning = meanings[number]
                words = MeaningSetter._remove_commas_from_relation_elements(result)
                meaning.relations[relations_type] = words

    @staticmethod
    def _remove_commas_from_relation_elements(relation_elements):
        return [word.replace(',', '') for word in relation_elements.elements]

    @staticmethod
    def set_translations_result(results_list: list, meanings: dict):
        for result in results_list:
            number = result.number
            if number in meanings:
                meaning = meanings[number]
                if result.language not in meaning.translations:
                    meaning.translations[result.language] = []
                meaning.translations[result.language].extend(result.translations)


def _is_meaning_section(section):
    return section == PolishParser.MEANINGS


def _is_examples_section(section):
    return section == PolishParser.EXAMPLES


def _is_relations_section(section):
    return section in PolishParser.POLISH_RELATIONS


def _is_translations_section(section):
    return section == PolishParser.TRANSLATIONS


def _create_result_list_from_map(result_map):
    return [value for key, value in result_map.items()]


def _is_empty_text_element(text_element):
    return text_element == '' or text_element == ',' or text_element.isspace()


class PolishParser(IParser):
    # TODO: poprawić to, aby wyciągało ostatnie znaki spoza nawiasów kwadratowych
    _EXAMPLE_PATTERN = '[\[\]]+ ?(.*?(\]\]\S+?)?) ?[\[\]]+?'
    _NUMBER_PATTERN = '\((.*?)\)'
    _TRANSLATION_NUMBER_PATTERN = '\* (.*?): \((.*?)\)'

    # TODO: te wartości można przerzucić do pliku konfiguracyjnego
    MEANINGS = '{{znaczenia}}'
    INFLECTION = '{{odmiana}}'
    SYNONYMS = '{{synonimy}}'
    ANTONYMS = '{{antonimy}}'
    HYPERNYMS = '{{hiperonimy}}'
    HYPONYMS = '{{hiponimy}}'
    HOLONYMS = '{{holonimy}}'
    MERONYMS = '{{meronimy}}'
    EXAMPLES = '{{przykłady}}'
    TRANSLATIONS = '{{tłumaczenia}}'

    MUSCULINE_TEXT = 'męsk'
    FEMININE_TEXT = 'żeńsk'
    NEUTRAL_TEXT = 'nijak'

    NOUN = 'rzeczownik'
    VERB = 'czasownik'
    ADJECTIVE = 'przymiotnik'
    ADVERB = 'przysłówek'

    # languages
    ENGLISH_LANGUAGE = 'angielski'
    SPANISH_LANGUAGE = 'hiszpański'

    POLISH_LANGUAGE_SECTION = 'język polski' # TODO: to można jeszcze przemyśleć
    ENGLISH_LANGUAGE_SECTION = 'język angielski'

    POLISH_RELATIONS = [SYNONYMS, ANTONYMS, HYPONYMS, HYPERNYMS, HOLONYMS, MERONYMS]
    RELATIONS_MAP = {SYNONYMS: IParser.SYNONYMS, ANTONYMS: IParser.ANTONYMS, HYPONYMS: IParser.HYPONYMS,
                     HYPERNYMS: IParser.HYPERNYMS, HOLONYMS: IParser.HOLONYMS, MERONYMS: IParser.MERONYMS}
    LANGUAGES_MAP = {ENGLISH_LANGUAGE: IParser.ENGLISH_LANGUAGE, SPANISH_LANGUAGE: IParser.SPANISH_LANGUAGE}
    LANGUAGES_SECTION_MAP = {Languages.POLISH: POLISH_LANGUAGE_SECTION, Languages.ENGLISH: ENGLISH_LANGUAGE_SECTION}

    def __init__(self):
        self._current_meaning = None

    def parse(self, text: str):
        # TODO: dodać wprowadzenie języka
        assert text
        # start_index, language = self._get_language_section_start_index(text, PolishParser.LANGUAGES_SECTION_MAP[Languages.POLISH])
        start_index, language = self._get_language_section_start_index(text, PolishParser.LANGUAGES_SECTION_MAP[Languages.ENGLISH])
        if start_index < 0:
            return None
        lines = text[start_index + 1:].splitlines()
        section_text = ''
        last_section = None
        meanings = {}
        for index, line in enumerate(lines):
            line = line.strip()
            if line.startswith(START_SECTION):
                if _is_meaning_section(last_section):
                    meaning_results = self._process_meanings(section_text)
                    MeaningSetter.set_meanings_result(meaning_results, meanings)
                elif _is_examples_section(last_section):
                    examples_result = self._process_examples(section_text)
                    MeaningSetter.set_examples_result(examples_result, meanings)
                elif _is_relations_section(last_section):
                    relations_result = self._process_relation(section_text)
                    MeaningSetter.set_relations_result(relations_result, meanings,
                                                       PolishParser.RELATIONS_MAP[last_section])
                elif _is_translations_section(last_section):
                    translation_result = self._process_translations(section_text)
                    MeaningSetter.set_translations_result(translation_result, meanings)
                section_text = ''
                last_section = line
            elif line.startswith(START_LANGUAGE_SECTION):
                return _create_result_list_from_map(meanings)
            else:
                section_text += line + '\n'
                # TODO: można wykorzystać takie samo podejście jak w przypadku angielskiego

        return _create_result_list_from_map(meanings)

    def _get_language_section_start_index(self, text, allowable_language, start_search_index=0):
        assert text
        assert start_search_index >= 0
        current_index = start_search_index
        while current_index > -1:
            current_index, language = self._match_language_section(text, current_index)
            if language == allowable_language:
                language_code = self._get_language_from_section(language)
                return current_index, language_code
        return -1, ''

    def _get_language_from_section(self, language_text):
        if language_text == PolishParser.POLISH_LANGUAGE_SECTION:
           return Languages.POLISH
        if language_text == PolishParser.ENGLISH_LANGUAGE_SECTION:
            return Languages.ENGLISH

    def _match_language_section(self, text, current_index):
        language_pattern = '==.*?\({{(.*?)}}\) =='
        language_match = re.search(language_pattern, text, pos=current_index)
        if language_match:
            language = language_match.group(1)
            index = language_match.end()
            return index, language
        return -1, ''


    def _parse_section(self, section_text):
        section_result = []
        for line in section_text.splitlines():
            line = line.rstrip()
            number, parse_result = self._parse_line(line)
            result = ElementResult(number, parse_result)
            section_result.append(result)

        return section_result

    def _parse_translation_section(self, section_text):
        section_result = []
        for line in section_text.splitlines():
            line = line.rstrip()
            language_text, translations_text = self._split_line_on_language_and_translations(line)
            language = self._get_language(language_text)
            if language:
                translations_results = self._parse_translations(language, translations_text)
                section_result.extend(translations_results)
            # TODO: sprawdzić, jak zachowa się w przypadku nie znalezienia języka
        return section_result

    def _split_line_on_language_and_translations(self, line):
        if self._is_translation_line(line):
            start_lang_index = line.index('*')
            if ':' not in line: # TODO: poprawić to
                return '',''
            end_lang_index = line.index(':')
            language_text = line[start_lang_index + 2: end_lang_index]
            translations_text = line[end_lang_index + 1:]
            return language_text, translations_text
        return '', ''

    def _is_translation_line(self, line):
        if len(line) == 0:
            return False
        return line[0] == '*'

    def _parse_translations(self, language, translation_line):
        section_result = []
        translations = translation_line.split(';')
        for translation in translations:
            number, parse_result = self._parse_line(translation)
            result = TranslationResult(number, language, parse_result)

            section_result.append(result)
        return section_result

    def _parse_line(self, line):
        number, parse_elements = self._parse_meaning_text(line)
        return number, parse_elements

    def _process_translations(self, section_text):
        if section_text.startswith(PolishParser.TRANSLATIONS):
            return []
        return self._parse_translation_section(section_text)

    def _process_examples(self, section_text):
        return self._parse_section(section_text)

    def _process_relation(self, section_text):
        return self._parse_section(section_text)

    def _parse_meaning_text(self, line):
        number = self._get_number(line)
        text_elements = self._get_text_elements(line)
        return number, text_elements

    # TODO: tę nazwę można by zmienić
    def _get_text_elements(self, line, with_coma=True):
        text_parts = []
        text_match = re.findall(PolishParser._EXAMPLE_PATTERN, line)
        for group in text_match:
            group_text = group[0]
            if _is_empty_text_element(group_text):
                continue
            text_part = self._get_text_part(group_text)
            text_part = self._clean_text(group_text, text_part, with_coma)
            if not text_part.isspace():  # TODO: sprawdzić, czy będzie poprawnie
                text_parts.append(text_part)

        return text_parts

    def _get_text_part(self, group_text):
        if '|' in group_text:
            parts = group_text.split('|')
            text_part = parts[1]
        else:
            text_part = group_text
        return text_part

    def _clean_text(self, group_text, text_part, with_coma):
        if ']]' in group_text:
            text_part = text_part.replace(']]', '')
        if not with_coma and ',' in text_part:
            text_part = text_part.replace(',', '')
        return text_part

    def _get_number(self, line):
        number_match = re.search(PolishParser._NUMBER_PATTERN, line)
        number = None
        if number_match:
            number = number_match.group(1)
        return number

    def _get_language_and_number(self, line):
        match = re.search(PolishParser._TRANSLATION_NUMBER_PATTERN, line)
        number = None
        language = None
        if match:
            number = match.group(2)
            language_text = match.group(1)
            language = self._get_language(language_text)
        return language, number

    def _get_language(self, language_text):
        if language_text in PolishParser.LANGUAGES_MAP:
            return PolishParser.LANGUAGES_MAP[language_text]
        return None

    def _parse_translation_text(self, line):
        language, number = self._get_language_and_number(line)
        text_elements = []
        if language:
            text_elements = self._get_text_elements(line)
        return language, number, text_elements

    def _process_meanings(self, section_text: str) -> list:
        meanings_result = []
        current_pos = None
        current_gender = None
        for line in section_text.splitlines():
            if self._is_pos_and_gender_line(line):
                current_gender, current_pos = self._get_gender_and_pos(section_text)
            elif self._is_definition_line(line):
                number, definition, registers = self._get_meaning(line)
                result = MeaningResult(number, current_pos, current_gender, registers, definition)
                meanings_result.append(result)
        return meanings_result

    def _is_pos_and_gender_line(self, line):
        return line.startswith("''")

    def _is_definition_line(self, line):
        return line.startswith(":")

    def _get_gender_and_pos(self, text: str):
        line_text = self._get_post_and_gender_part(text)
        gender = self._get_gender(line_text)
        pos = self._get_pos(line_text)
        return gender, pos

    def _get_post_and_gender_part(self, text):
        line_text = text.strip().split('\n', 1)[0]
        line_text = line_text.replace("''", "")
        return line_text

    def _get_gender(self, line_text):
        if PolishParser.MUSCULINE_TEXT in line_text:
            gender = Gender.MUSCULINE
        elif PolishParser.FEMININE_TEXT in line_text:
            gender = Gender.FEMININE
        elif PolishParser.NEUTRAL_TEXT in line_text:
            gender = Gender.NEUTRAL
        else:
            gender = Gender.LACK
        return gender

    def _get_pos(self, line_text):
        if PolishParser.NOUN in line_text:
            pos = PartOfSpeech.NOUN
        elif PolishParser.VERB in line_text:
            pos = PartOfSpeech.VERB
        elif PolishParser.ADJECTIVE in line_text:
            pos = PartOfSpeech.ADJECTIVE
        elif PolishParser.ADVERB in line_text:
            pos = PartOfSpeech.ADVERB
        else:
            pos = PartOfSpeech.LACK
        return pos

    def _get_meaning(self, text: str):
        registers = self._get_registers(text)
        number = self._get_number(text)
        definition = self._get_definition(text)
        return number, definition, registers

    def _get_definition(self, text):
        definition_match = re.findall(PolishParser._EXAMPLE_PATTERN, text)
        definition_elements = []
        for group in definition_match:
            group = group[0].replace(']]', '')
            definition_part = self._get_text_part(group)
            definition_elements.append(definition_part)
        definition = ' '.join(definition_elements)
        return definition

    def _get_registers(self, text):
        register_pattern = '.*?{{(.*?)}}.*?'
        register_match = re.findall(register_pattern, text)
        registers_elements = []
        for register in register_match:
            if self._is_correct_register(register):
                registers_elements.append(register)
        return registers_elements

    def _is_correct_register(self, register):
        return register != 'wikipedia' \
               and '|' not in register

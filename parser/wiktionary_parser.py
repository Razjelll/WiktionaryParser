import re

import xml.etree.ElementTree as ET

from parser.english_parser import EnglishParser
from parser.polish_parser import PolishParser
from parser.values import Languages
import abc

_TITLE = 'title'
_START = 'start'
_TEXT = 'text'
_END = 'end'

class WiktionaryParser:

    def __init__(self, parser_language):
        assert parser_language in [Languages.POLISH, Languages.ENGLISH]
        self.__language = parser_language
        self.__parser = self._get_parser(parser_language)


    def parse(self, xml_path):
        result = {}
        events  = ('start','end')
        tag_regex_pattern = '.*}(.*)$'
        last_page_title = None

        xmlns_length = -1
        for event, elem in ET.iterparse(xml_path, events=events):
            # TODO: sprawdzić, czy da się to zrobić przed parsowaniem
            if not self._is_known_xmlns_length(xmlns_length):
                xmlns_length = self._get_xmlns_length(elem)

            tag = elem.tag[xmlns_length:]
            if self._is_start_title(tag, event):
                last_page_title = elem.text
                if last_page_title and '/' in last_page_title:
                    print()
            elif self._is_end_text(tag, event):
                # TODO: przemyśleć to i napisać lepiej
                if elem.text:
                    if '/' in last_page_title:
                        title = last_page_title.split('/')[0]
                        if title in result:
                            parse_result = self.__parser.parse(elem.text, result[title])
                            if parse_result:
                                result[title] = parse_result
                        else:
                            print('Nie ma takiego czegoś ' + title)
                    else:
                        parse_result = self.__parser.parse(elem.text)
                        if parse_result:
                            result[last_page_title] = parse_result
            elem.clear()
            # TODO: w przypadku anguiels
        if self.__language == Languages.ENGLISH:
            pass
        return result

    # TODO: metoda tymczasowa. Umieścić to w innym miejscu
    def _connect_translations_to_meaning(self, parser_result):
        for title, meanings in parser_result.items():
            if '/' in title:
                splited_title = title.split('/')
                title = splited_title[0]
                if splited_title[1] == 'translations' and title in parser_result:
                    title_meanings = parser_result[title]
                    translations = []



    def _is_start_title(self, tag, event):
        return tag == _TITLE and event == _START

    def _is_end_text(self, tag, event):
        return tag == _TEXT and event == _END

    def _is_known_xmlns_length(self, xmlns_length):
        return xmlns_length > 0

    def _get_xmlns_length(self, elem):
        xmlns_length = elem.tag.index('}') + 1 if '}' in elem.tag else 0
        return xmlns_length

    def _get_parser(self, language):
        parser = None
        if language == Languages.POLISH:
            parser = PolishParser()
        elif language == Languages.ENGLISH:
            parser = EnglishParser()
        return parser



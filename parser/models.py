class Meaning:

    def __init__(self, pos=None, definition=None):
        self.part_of_speech = pos
        self.gender = None
        self.registers = []
        self.definition = definition
        self.examples = []
        self.relations = {}
        self.translations = {}


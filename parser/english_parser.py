import re

from parser.i_parser import IParser
from parser.models import Meaning
from parser.values import PartOfSpeech
from utils.definition_similarity import DefinitionSimilarity

TRANSLATIONS = 'Translations'


class Translation:

    def __init__(self, definition, language, translations):
        self.definition = definition
        self.language = language
        self.translations = translations


class EnglishParser(IParser):
    ENGLISH_SECTION = '==English=='
    LANGUAGE_START = '=='
    MEANINGS_START = '==='
    INFO_START = '===='

    EXAMPLES_START_SYMBOL = '#*'
    RELATION_START_SYMBOL = '#:'
    DEFINITION_START_SYMBOL = '#'

    INFO_PATTERN = '====(\w*)===='
    REGISTER_MEANINGS_PATTERN = '{{(.*?)}}'
    # DEFINITION_PATTERN = '}} (.*?)$'
    DEFINITION_PATTERN = '}} ?(.*?)$'
    DEFINITION_PATTERN_2 = '# ?(.*?)$'
    EXAMPLE_PATTERN = 'passage=(.*?)\}\}'
    RELATION_PATTERN = '#: {{(.*?)\|.*?\|(.*?)}}'
    LANGUAGE_PATTERN = '\* (.*?):'
    TRANSLATION_PATTERN = '{{.*?\|\w*\|(.*?)}}'
    TRANSLATION_MEANING_PATTERN = '{{trans-top\|(.*?)}}'
    SECTION_LANGUAGE_PATTERN = '==(.*?)=='

    POS_MAP = {'Noun': PartOfSpeech.NOUN,
               'Verb': PartOfSpeech.VERB,
               'Adverb': PartOfSpeech.ADVERB,
               'Adjective': PartOfSpeech.ADJECTIVE}

    LANGUAGE_MAP = {'Polish': IParser.POLISH_LANGUAGE,
                    'English': IParser.ENGLISH_LANGUAGE}

    RELATION_MAP = {'syn': IParser.SYNONYMS}

    # TODO: sprawdzić, czy indeksy są poprawne i niczego się po drodze nie gubi
    # TODO: refaktoryzacja całego angielskiego parsera
    def parse(self, text: str, connected_meanings=None):
        if text is None or EnglishParser.ENGLISH_SECTION not in text:
            return []
        lines = text.splitlines()
        current_line_index = 0

        current_language = None
        ENGLISH = 'English'
        result = []
        while current_line_index < len(lines):
            line = lines[current_line_index].strip()
            if self._is_info_section(line):
                if current_language == ENGLISH:
                    # self._process_info(current_line_index, lines, result)
                    # self._process_info(current_line_index, lines, result) #TODO: sprawdzić, czy indeksy się zgadzają
                    # TODO: przyjrzeć się temu z bliska. Strasznie to mylące
                    if connected_meanings:
                        self._process_info(current_line_index, lines, connected_meanings)
                        print()
                    else:
                        self._process_info(current_line_index, lines, result)
            elif self._is_meaning_section(line):
                if current_language == ENGLISH and self._contains_part_of_speech(line):
                    if connected_meanings:
                        # TODO: prawdopodobnie w tym miejscu nie będzie się robić nic
                        # meanings, index = self._parse_meanings(lines, current_line_index, connected_meanings)
                        # result = meanings
                        pass
                    else:
                        meanings, index = self._parse_meanings(lines, current_line_index)
                        result.extend(meanings)
                    # meanings, index = self._parse_meanings(lines, current_line_index)
                        current_line_index = index
            elif self._is_language(line):
                current_language = self._get_section_langauge(line)
            else:
                pass
                # TODO: zakończenie iz zwrócenie wyniku, chyba
            current_line_index += 1
        return result

    def _get_section_langauge(self, line: str) -> str:
        language_match = re.search(EnglishParser.SECTION_LANGUAGE_PATTERN, line)
        if language_match:
            language = language_match.group(1)
            return language
        return 'LACK'

    def _is_language(self, line:str):
        return line.startswith(EnglishParser.LANGUAGE_START)

    def _is_info_section(self, line: str):
        return line.startswith(EnglishParser.INFO_START)

    def _is_meaning_section(self, line: str):
        return line.startswith(EnglishParser.MEANINGS_START)

    def _contains_part_of_speech(self, line):
        pos = self._get_part_of_speech_value(line)
        return pos != PartOfSpeech.LACK

    def _get_part_of_speech_value(self, text):
        for pos_text, pos_value in EnglishParser.POS_MAP.items():
            if pos_text in text:
                return pos_value
        return PartOfSpeech.LACK

    def _process_info(self, current_line_index, lines, result):
        line = lines[current_line_index]
        info_type = self._get_info_type(line)
        if info_type == TRANSLATIONS:
            if len(result) > 0:
                translations = self._parse_translations(lines, current_line_index)
                self._connect_translation_to_meaning(result, translations)

    def _connect_translation_to_meaning(self, meanings, translations):
        translation_definitions_map = self._create_translations_definitions_map(translations)

        used_meanings = set()
        for definition, indices in translation_definitions_map.items():
            if definition is None:
                print()
            best_meaning = self._find_best_translation_fit(definition, meanings, used_meanings)
            if not best_meaning:
                continue
            translations_map = self._create_translations_map(indices, translations)
            best_meaning.translations = translations_map
            used_meanings.add(best_meaning)

    def _create_translations_map(self, indices, translations):
        translations_elements = [translations[index] for index in indices]
        translations_map = self._get_translations_map(translations_elements)
        return translations_map

    def _find_best_translation_fit(self, definition, meanings, used_meanings):
        best_meaning: Meaning = None
        best_similarity = 0
        for meaning in meanings:
            if meaning in used_meanings:
                continue
            if meaning.definition is None or definition is None:
                print()
            similarity_score = DefinitionSimilarity.get_definition_similarity_score(meaning.definition, definition)
            if similarity_score > best_similarity:
                best_similarity = similarity_score
                best_meaning = meaning
        return best_meaning

    def _create_translations_definitions_map(self, translations):
        translation_definitions_map = {}
        for index, translation in enumerate(translations):
            definition = translation.definition
            if definition is None:
                continue
            if definition not in translation_definitions_map:
                translation_definitions_map[definition] = []
            translation_definitions_map[definition].append(index)
        return translation_definitions_map

    def _get_translations_map(self, translations):
        translations_result = {}
        for translation in translations:
            translations_result[translation.language] = translation.translations
        return translations_result

    def _get_info_type(self, line: str):
        info_match = re.search(EnglishParser.INFO_PATTERN, line)
        if info_match:
            return info_match.group(1)
        return None

    def _parse_meanings(self, lines, start_index, connected_meanings=[]):
        index = start_index
        if index >= len(lines):  # TODO: sprawdzić to
            return [], index
        current_line = lines[index]
        pos_text = current_line.replace('==', '')
        part_of_speech = self._get_part_of_speech(pos_text)
        current_meaning: Meaning = None
        index += 1
        if index >= len(lines):
            return [], index  # TODO: zobaczyć to
        current_line = lines[index]
        meanings_result = []

        # TODO: koniecznie zrobić refaktoryzację
        while not current_line.startswith('==') and not current_line.startswith('--') and index < len(lines):
            if self._is_examples_line(current_line):
                example = self._parse_example(current_line)
                if current_meaning is not None:  # TODO: sprawdzić to
                    current_meaning.examples.append(example)
            elif self._is_relation_line(current_line):
                relation, related_word = self._parse_relation(current_line)
                if current_meaning is not None:  # TODO: sprawdzić to
                    if relation not in current_meaning.relations:
                        current_meaning.relations[relation] = []
                    current_meaning.relations[relation].append(related_word)
            elif self._is_definition_line(current_line):
                if current_meaning is not None:
                    meanings_result.append(current_meaning)  # append previous meaning
                    current_meaning = None
                register, definition = self._parse_meaning(current_line)
                if definition:
                    current_meaning = Meaning(part_of_speech, definition)
                    current_meaning.register = register
            index += 1
            if index < len(lines):
                current_line = lines[index]
        # append last element
        if current_meaning is not None:
            meanings_result.append(current_meaning)

        return meanings_result, index

    def _is_examples_line(self, line):
        return line.startswith(EnglishParser.EXAMPLES_START_SYMBOL)

    def _is_relation_line(self, line):
        return line.startswith(EnglishParser.RELATION_START_SYMBOL)

    def _is_definition_line(self, line):
        return line.startswith(EnglishParser.DEFINITION_START_SYMBOL) and not line.startswith('##')

    def _parse_relation(self, line: str):
        relation_match = re.match(EnglishParser.RELATION_PATTERN, line)
        if relation_match:
            relation_text = relation_match.group(1)
            relation = self._get_relation(relation_text)
            related_word = relation_match.group(2)

            return relation, related_word
        return None, None

    def _get_relation(self, relation_text: str):
        if relation_text in EnglishParser.RELATION_MAP:
            return EnglishParser.RELATION_MAP[relation_text]
        return None

    def _parse_meaning(self, line):
        register = self._get_registers(line)
        definition = self._get_definition(line)
        return register, definition

    def _get_definition(self, line):
        if len(line) < 2:  # TODO: sprawdzić to
            return ''
        definition_pattern = EnglishParser.DEFINITION_PATTERN if line[2] == '{' else EnglishParser.DEFINITION_PATTERN_2
        definition_match = re.search(definition_pattern, line)
        definition = None
        if definition_match:
            definition = definition_match.group(1)
            definition = self._clean_definition(definition)
        if definition is None:
            print()
        return definition

    def _clean_definition(self, definition):
        definition = definition.replace(']]', '').replace('[[', '')
        return definition

    def _get_registers(self, line):
        register_match = re.search(EnglishParser.REGISTER_MEANINGS_PATTERN, line)
        if register_match:
            register = register_match.group(1)
            register = self._clean_register(register)
            registers = register.split('|') # TODO: sprawdzić,cz
            return registers
        return []

    def _clean_register(self, register:str):
        register.strip()
        register = register.replace('lb|en|', '')
        return register

    def _parse_example(self, line: str):
        example_match = re.search(EnglishParser.EXAMPLE_PATTERN, line)
        if example_match:
            example = example_match.group(1)
            example = self._clean_example(example)
            return example
        return None

    def _clean_example(self, example):
        example = example.replace("'''", '')
        return example

    def _get_part_of_speech(self, pos_text):
        pos = self._get_part_of_speech_value(pos_text)
        return pos

    def _parse_translations(self, lines, start_index):
        index = start_index + 1  # TODO: sprawdzić to
        current_line = lines[index]
        current_meaning = None

        translations_result = []

        while not current_line.startswith('==') and not current_line.startswith('--'):
            if self._is_start_translations_line(current_line):  # meaning
                current_meaning = self._get_translation_meaning(current_line)
            elif self._is_translation_line(current_line):
                language, translations = self._parse_translation_line(current_line)
                if language and translations:
                    translation = Translation(current_meaning, language, translations)
                    translations_result.append(translation)
            elif self._is_other_translation_line(current_line):
                pass  # TODO zastanowić się, czy tutaj czegoś nie powinno być
            index += 1
            if len(lines) <= index:
                return translations_result
            current_line = lines[index]
        return translations_result

    def _is_start_translations_line(self, line):
        return line.startswith('{{trans-top')

    def _is_translation_line(self, line):
        return line.startswith('*')

    def _is_other_translation_line(self, line):
        return line.startswith('{{')

    def _get_translation_meaning(self, line: str):
        match = re.search(EnglishParser.TRANSLATION_MEANING_PATTERN, line)
        if match:
            definition = match.group(1)
            definition = self._clean_definition(definition)  # TODO: sprawdzić, czy to czyszczenie jest dobrze zrobione
            return definition
        return None

    def _parse_translation_line(self, line):
        language = self._get_translation_language(line)
        if language:
            translations = self._get_translations(line)
            return language, translations
        return None, None

    # TODO: zastanowić się nad nazwą
    def _get_translations(self, line: str):
        translations = []
        translation_match = re.findall(EnglishParser.TRANSLATION_PATTERN, line)
        if translation_match:
            for translation in translation_match:
                translation = translation.split('|')[0]
                translations.append(translation)  # TODO: obsłużyć |f
        return translations

    def _get_translation_language(self, line):
        language_match = re.search(EnglishParser.LANGUAGE_PATTERN, line)
        language = None
        if language_match:
            language_text = language_match.group(1)
            language = self._get_language(language_text)
        return language

    def _get_language(self, language_text: str):
        if language_text in EnglishParser.LANGUAGE_MAP:
            return EnglishParser.LANGUAGE_MAP[language_text]
        return None

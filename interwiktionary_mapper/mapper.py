import pickle

class InterwiktionaryMapper:

    def start(self, first_wiki_path, second_wiki_path):
        # załadowanie wpisów z polskiego wikisłownika
        # załadowanie wspiów z angielskiego wikisłownika
        # dla każdego polskiego wpisu znaleźć wszystkie wspiy z angielskiego o takiej samej nazwie
        # porównać tłumaczenia
        # porównać definicje za pomocą lasera

        first_wiki = self._load_parser_result(first_wiki_path)
        second_wiki = self._load_parser_result(second_wiki_path)

        result_map = {}
        for word, meanings in first_wiki.items():
            second_wiki_meanings = self._find_meanings_by_word(word, second_wiki)
            for meaning in meanings:
                best_score = 0
                best_meaning = None
                for second_meaning in second_wiki_meanings:
                    score = self._score_translation_similarity(meaning, second_meaning)
                    if score > best_score:
                        best_score = score
                        best_meaning = second_meaning
                result_map[meaning] = best_meaning


    def _load_parser_result(self, path):
        return pickle.load(open(path, 'rb'))

    def _find_meanings_by_word(self, word, wiki):
        if word in wiki:
            return wiki[word]
        return []

    def _score_translation_similarity(self, first_meaning, second_meaning):
        score = 0
        for language, translations in first_meaning.translations.items():
            if language not in second_meaning.translations:
                continue
            for translation in translations:
                if translation in second_meaning.translations[language]:
                    score += 1
        return score

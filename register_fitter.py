
class RegisterFitter:

    @staticmethod
    def fit(polish_path, english_path):
        result = {}
        polish_registers = RegisterFitter._read_file(polish_path)
        english_registers = RegisterFitter._read_file(english_path)
        translations_map = RegisterFitter._get_translation_map(english_registers)

        for english_register in english_registers:
            translated_register = translations_map[english_register]
            best_register = RegisterFitter._get_best_register(polish_registers, translated_register)
            if best_register:
                result[english_register] = best_register
        return result

    @staticmethod
    def _get_translation_map(english_registers):
        translations_path = 'output/translations.txt'
        translations = RegisterFitter._read_file(translations_path)

        translations_map = {}
        for index, register in enumerate(english_registers):
            translation = translations[index]
            translations_map[register] = translation
        return translations_map


    @staticmethod
    def _read_file(path):
        result = []
        with open(path, 'r') as file:
            for line in file:
                word = line.split(':')[0]
                result.append(word)
        return result

    @staticmethod
    def _score_similarity(source_text:str, target_text:str):
        counter = 0
        for index, source_char in enumerate(source_text):
            if index >= len(target_text):
                return counter
            target_char = target_text[index]
            if source_char == target_char:
                counter += 1
            else:
                return counter
        return counter

    @staticmethod
    def _get_best_register(registers, register_translation):
        best_register = None
        best_score = -1
        for register in registers:
            score = RegisterFitter._score_similarity(register, register_translation)
            if score > best_score and score == len(register):
                best_register = register
                best_score = score
        return best_register

    @staticmethod
    def clean_register_file(input_path, output_path):
        lines = []
        with open(input_path, 'r') as input_file:
            for line in input_file:
                line = RegisterFitter._clean_register(line)
                lines.append(line)

        with open(output_path, 'w') as output_file:
            for line in lines:
                output_file.write(line)

    @staticmethod
    def _clean_register(register):
        cleanded_register = register.replace('[', '')\
            .replace(']','')\
            .replace('"','')\
            .replace('{', '')\
            .replace('}','')\
            .replace(' ','')
        return cleanded_register

    @staticmethod
    def save_register_mappings_to_file(registers_mapping, output_path):
        with open(output_path, 'w') as file:
            for key, value in registers_mapping.items():
                file.write(key)
                file.write(':')
                file.write(value)
                file.write('\n')

    @staticmethod
    def get_registers_mappings_from_file(input_path):
        result = {}
        with open(input_path, 'r') as file:
            for line in file:
                line = line.rstrip()
                parts = line.split(':')
                key = parts[0]
                value = parts[1]
                result[key] = value
        return result

    @staticmethod
    def get_register_number(registers_mappers):
        '''Zwraca numer rejestru na podstawie ...'''
        cleaned_mappings = RegisterFitter._remove_incorrect(registers_mappers)
        # cleaned_mappings = {v: k for k, v in cleaned_mappings.items()}

        result_map = {}
        counter = 0
        conflicts = []
        for english_register, polish_register in cleaned_mappings.items():
            # assert english_register not in result_map, english_register
            if english_register in result_map:
                conflicts.append((english_register, polish_register))
            result_map[english_register] = counter
            if english_register != polish_register \
                    and polish_register != 'dial': # TODO: to jest do usunięcia
                # assert polish_register not in result_map, polish_register
                if polish_register in result_map:
                    conflicts.append((english_register, polish_register))
                result_map[polish_register] = counter

        return result_map

    @staticmethod
    def _remove_incorrect(registers_mappings):
        # TODO: zmienić nazwę
        result = {}
        for key, value in registers_mappings.items():
            if len(value) > 1:
                result[key] = value
        return result

    @staticmethod
    def find_registers_mapping_conflict(registers_mapping):
        registers_map = {}
        for key,value in registers_mapping.items():
            if value not in registers_map:
                registers_map[value] = []
            registers_map[value].append(key)

        registers_with_conflict = []
        for key, value in registers_map.items():
            if len(value) > 1:
                registers_with_conflict.append(key)
        return registers_with_conflict

    @staticmethod
    def connect_registers(registers_mappings, english_parser_result, polish_parser_result):
        # 1. pogrupować jednostki ze względu na rejestr
        # 2. dla każdego rejestru wybrać wszystkie jednostki angielskie z tłumaczeń
        # 3. zrobić zliczanie rejestrów
        # 4. sprawdzić, czy tłumaczenie znajduje się w słowniku

        registers_map = RegisterFitter.__group_registers(polish_parser_result)
        for register, values in registers_map.items():
            translated_units = RegisterFitter.__find_english_translations_for_units(values, english_parser_result)
            counted_registers = RegisterFitter.__count_registers(translated_units)
            unit = RegisterFitter.__find_correct_register(counted_registers, registers_mappings)
            print()

        # # TODO: dla każdego polskiego rejestru wziąć wszystkie znaczenia
        # # TODO: dla każdego rejestru zebrać wszystkie rejestry z tłumaczeń
        # result = {}
        # for polish_word, polish_meanings_list in polish_parser_result.items():
        #     for meaning in polish_meanings_list:
        #         if meaning.register is not None:
        #             if len(meaning.translations) > 1:
        #                 print()
        #
        #             register = RegisterFitter._clean_register(meaning.register)
        #             if register and  'en' in meaning.translations:
        #                 translations = meaning.translations['en']
        #                 english_registers = RegisterFitter._get_english_registers(english_parser_result, translations)
        #                 print(english_registers)
        #                 # TODO: dla każdego tłumaczenia
        #
        #             # TODO: zrobić czyszczenie rejestru
        #             # TODO: angielskie rejestry będzie trzeba oddzielić

    @staticmethod
    def __group_registers(units:dict):
        registers_map = {}
        for word, values in units.items():
            for unit in values:
                register = unit.register
                if register:
                    register = RegisterFitter._clean_register(register) # TODO: sprawdzić, czy metoda czyści także polskie rejestry
                    if register not in registers_map:
                        registers_map[register] = []
                    registers_map[register].append(unit)
        return registers_map

    @staticmethod
    def __find_english_translations_for_units(units:list, english_parser_result):
        # TODO: pomyśleć nad nazwą
        result_units = []
        for unit in units:
            translations = RegisterFitter.__get_english_translations(unit)
            for translation in translations:
                if translation in english_parser_result:
                    translated_units = english_parser_result[translation]
                    result_units.extend(translated_units)
        return result_units

    @staticmethod
    def __count_registers(translated_units):
        count_result = {}
        for unit in translated_units:
            register_text = unit.register
            if register_text:
                registers = RegisterFitter.__get_english_registers(register_text)
                for register in registers:
                    register = RegisterFitter._clean_register(register)
                    if register not in count_result:
                        count_result[register] = 0
                    count_result[register] += 1
        return count_result

    @staticmethod
    def __get_english_registers(register):
        if register:
            register = register.replace('lb|en|','')
            splited_register = register.split('|')
            return splited_register

    @staticmethod
    def __find_correct_register(translated_units, translations):
        sorted_registers = sorted(translated_units, key=translated_units.__getitem__, reverse=True)
        print()

    @staticmethod
    def __get_english_translations(unit):
        # TODO: sprawdzić, czy nie wielu tłumaczeń dla nadego języka
        if 'en' in unit.translations:
            return unit.translations['en']
        return []

    @staticmethod
    def _get_english_registers(english_parser_result, translations):
        english_registers = set()
        for translation in translations:
            if translation in english_parser_result:
                for english_meaning in english_parser_result[translation]:
                    if english_meaning.register is not None:
                        english_register = english_meaning.register
                        if 'lb|en|' in english_register:
                            english_register = RegisterFitter._clean_register(english_register)
                            english_registers.add(english_register)
        return english_registers

    # TODO: przetestować to wszystko

    @staticmethod
    def _read_register_map(path):
        ''' Odczytywanie pliku z przypisanymi ręcznie rejestrami'''
        result_map = {}
        with open(path, 'r') as file:
            for line in file:
                line = line.rstrip()
                splited_line = line.split(':')
                if len(splited_line) == 2:
                    english_register = splited_line[0]
                    polish_register = splited_line[1]
                    result_map[english_register] = polish_register
                    result_map[polish_register] = polish_register
        return result_map

    @staticmethod
    def _get_meaning_register_map(parser_result, register_map, register_numbers:bool):
        if register_numbers:
            register_map = RegisterFitter._prepare_registers_number_map(register_map)
        ''' Tworzy przypisanie rejestrów do znaczeń'''
        result_register_map = {}
        for word, meanings in parser_result.items():
            for meaning in meanings:
                register = meaning.register
                if register in register_map:
                    correct_register = register_map[register]
                    result_register_map[meaning] = correct_register
        return result_register_map

    @staticmethod
    def _prepare_registers_number_map(meaning_register_map:dict):
        ''' Zamienia tekstowe rejestry na wartości liczbowe'''
        values = meaning_register_map.values()
        number_map = {}
        for index, value in enumerate(values):
            if value not in number_map:
                number_map[value] = index

        meanings_registers_result = {}
        for org_register, register_value in meaning_register_map.items():
            number_register = number_map[register_value]
            meanings_registers_result[org_register] = number_register
        return meanings_registers_result

